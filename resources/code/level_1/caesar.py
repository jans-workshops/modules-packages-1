"""Module provides simple version of caesar cipher"""


def encode(shift: int, text: str) -> str:
    """
    Encodes text with caesar cipher.

    Parameters
    ----------
    shift
        Number of characters to shift.

    text
        Text to encode

    Returns
    -------
    str
        Text shifted by shift characters.

    """

    shifted_numbers = map(lambda char: ord(char) + shift, text)
    return ''.join(map(lambda num: chr(num), shifted_numbers))


def decode(shift, text):
    """
    Decodes caesar cipher.

    Parameters
    ----------
    shift
        Number of characters to shift.

    text
        Text to decode

    Returns
    -------
    str
        Decoded text.

    """
    shifted_numbers = map(lambda char: ord(char) - shift, text)
    return ''.join(map(lambda num: chr(num), shifted_numbers))


if __name__ == '__main__':
    message = "Your secret text."
    cipher = encode(4, message)
    # use encoded message as input
    decoded = decode(4, cipher)

    print(f"Oridignal message: {message}")
    print(f"Encoded message: {cipher}")
    print(f"Decoded message: {decoded}")