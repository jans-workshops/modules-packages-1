import caesar

if __name__ == '__main__':
    message = "We call the caesar module!"
    cipher = caesar.encode(4, message)
    # use encoded message as input
    decoded = caesar.decode(4, cipher)

    print(f"Oridignal message: {message}")
    print(f"Encoded message: {cipher}")
    print(f"Decoded message: {decoded}")