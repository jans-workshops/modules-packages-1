from ciphers import caesar

if __name__ == "__main__":

    message = "I'm using ciphers package"

    encoded = caesar.encode(4, message)

    # use encoded message as input
    decoded = caesar.decode(4, encoded)

    print(f"Oridignal message: {message}")
    print(f"Encoded message: {encoded}")
    print(f"Decoded message: {decoded}")