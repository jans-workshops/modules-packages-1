"""Module provides simple version of caesar cipher"""


def encode(shift: int, text: str) -> str:
    """
    Encodes text with caesar cipher.

    Parameters
    ----------
    shift
        Number of characters to shift.

    text
        Text to encode

    Returns
    -------
    str
        Text shifted by shift characters.

    """

    shifted_numbers = map(lambda char: ord(char) + shift, text)
    return "".join(map(lambda num: chr(num), shifted_numbers))


def decode(shift, text):
    """
    Decodes caesar cipher.

    Parameters
    ----------
    shift
        Number of characters to shift.

    text
        Text to decode

    Returns
    -------
    str
        Decoded text.

    """
    shifted_numbers = map(lambda char: ord(char) - shift, text)
    return "".join(map(lambda num: chr(num), shifted_numbers))
