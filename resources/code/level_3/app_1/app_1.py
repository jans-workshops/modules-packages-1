import ciphers
from ciphers import caesar

if __name__ == "__main__":

    print("We are using package ciphers loaded from", ciphers)
    message = "Hello World!"

    encoded = caesar.encode(4, message)

    # use encoded message as input
    decoded = caesar.decode(4, encoded)

    print(f"Oridignal message: {message}")
    print(f"Encoded message: {encoded}")
    print(f"Decoded message: {decoded}")